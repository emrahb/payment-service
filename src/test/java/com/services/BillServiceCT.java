package com.services;


import au.com.dius.pact.provider.junit.PactRunner;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.State;
import au.com.dius.pact.provider.junit.loader.PactBroker;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.IOException;

@RunWith(PactRunner.class) // Say JUnit to run tests with custom Runner
@Provider("payment_provider") // Set up name of tested provider
@PactBroker(host = "${PACT_BROKER_HOST:localhost}", port = "80")
public class BillServiceCT {
    @TestTarget
    public final Target target;

    public BillServiceCT() {
        String host = System.getenv("PROVIDER_HOST");
        String port = System.getenv("PROVIDER_PORT");
        if (host == null) {
            host = "localhost";
        }
        if (port == null) {
            port = "8080";
        }
        target = new HttpTarget(host, Integer.parseInt(port));
    }

    @BeforeClass
    public static void setUpService() throws IOException {
    }

    @State("test state")
    public void toTestState() {
        System.out.println("Now service in test state");
    }

    @State("default")
    public void toDefaultState() {
        System.out.println("Now service in default state");
    }
}

