package com.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

/**
 * Created by molgun on 23/05/2017.
 */
@RestController
public class AccountController {
    @RequestMapping(method = RequestMethod.POST, value = "/takeProvision", consumes = "application/json")
    public ResponseEntity takeProvision(@RequestBody PaymentDTO dto) {
        return ResponseEntity.created(URI.create("http://localhost:8080")).build();
    }
}
