package com.services;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by molgun on 16/05/2017.
 */
public class Account {

    private String accountNumber;

    public Account() {
    }

    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(accountNumber)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Account account = (Account) o;

        return sameValueAs(account);
    }

    public boolean sameValueAs(Account account) {
        return account != null && new EqualsBuilder()
                .append(accountNumber, account.getAccountNumber())
                .isEquals();
    }
}
