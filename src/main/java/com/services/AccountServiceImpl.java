package com.services;


/**
 * Created by molgun on 18/05/2017.
 */
public class AccountServiceImpl implements AccountService {

    private AccountEvents accountEvents;

    public AccountServiceImpl(AccountEvents accountEvents) {
        this.accountEvents = accountEvents;
    }

    public void takeProvision(Account account, Amount amount, TrackingId trackingId) {
        if (Math.random() % 2 == 0) {
            accountEvents.couldNotProvisioned(trackingId);
        }
    }
}
