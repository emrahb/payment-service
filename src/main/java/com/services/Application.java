package com.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
@EnableEurekaClient
public class Application {

    @Value("${server.port}")
    int port;

    @RequestMapping("/")
    public String index(){
        return "Payment service running on port: " +  this.port;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
