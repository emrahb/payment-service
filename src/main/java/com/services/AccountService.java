package com.services;

/**
 * Created by molgun on 18/05/2017.
 */
public interface AccountService {

    public void takeProvision(Account account, Amount amount, TrackingId trackingId);
}
