FROM openjdk:8
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/payment-service/payment-service.jar"]

ADD target/payment-service.jar /usr/share/payment-service/payment-service.jar